<?php

namespace Tests\Feature;

use App\Models\Coffee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProcessXmlTest extends TestCase
{
	
	use RefreshDatabase;
	
	/**
	 * Checks that ProcessXml Command fails when file doesn't exist
	 */
	public function test_process_xml_command_fails_if_xml_file_does_no_exist(): void
	{
		$xmlFilePath = storage_path('app/xml/wrong_feed.xml');
		
		$this->artisan(
			'xml:process',
			[
				'file' => $xmlFilePath
			]
		)
		     ->assertFailed()
			->expectsOutput("File not found: $xmlFilePath");
		
		$this->assertDatabaseCount(
			Coffee::class,
			0
		);
	}
	
	/**
	 * Checks that ProcessXml Command fails when file is not xml type
	 */
	public function test_process_xml_command_fails_if_file_is_not_xml_type(): void
	{
		$xmlFilePath = storage_path('app/xml/wrong_type.txt');
		
		$this->artisan(
			'xml:process',
			[
				'file' => $xmlFilePath
			]
		)
		     ->assertFailed()
		     ->expectsOutput("Invalid file type: $xmlFilePath. Please provide an XML file.");
		
		$this->assertDatabaseCount(
			Coffee::class,
			0
		);
	}
	
    /**
     * Checks that ProcessXml Command adds record to database
     */
	public function test_process_xml_command_adds_records_to_database(): void
	{
		$xmlFilePath = storage_path('app/xml/feed.xml');
		
		$this->artisan(
			'xml:process',
			[
				'file' => $xmlFilePath
			]
		)
		     ->assertSuccessful()
			->expectsOutput("XML file processed successfully.");
		
		$this->assertDatabaseCount(
			Coffee::class,
			3449
		);
		
		$this->assertDatabaseHas(
			Coffee::class,
			[
				'name' => 'Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag'
			]
		);
	}
}
