<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('coffees', function (Blueprint $table) {
	        $table->id();
	        $table->integer('entity_id');
	        $table->string('category_name');
	        $table->string('sku');
	        $table->string('name');
	        $table->text('description')->nullable();
	        $table->text('shortdesc')->nullable();
	        $table->decimal('price', 8, 2);
	        $table->string('link');
	        $table->string('image');
	        $table->string('brand');
	        $table->integer('rating');
	        $table->string('caffeine_type')->nullable();
	        $table->integer('count')->nullable();
	        $table->boolean('flavored')->default(false);
	        $table->boolean('seasonal')->default(false);
	        $table->boolean('instock')->default(false);
	        $table->boolean('facebook')->default(false);
	        $table->boolean('is_kcup')->default(false);
	        $table->timestamps();
        });
    }
};
