<?php

namespace App\Console\Commands;

use App\Models\Coffee;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProcessXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xml:process {file}';
	
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process an XML file and push data to the database';
	
	public function __construct()
	{
		parent::__construct();
	}
	
    /**
     * Execute the console command.
     */
    public function handle()
    {
	    $file = $this->argument('file');
	    
	    if (!file_exists($file)) {
		    Log::error("File not found: $file");
		    $this->error("File not found: $file");
		    return 1;
	    }
	    
	    if (pathinfo($file, PATHINFO_EXTENSION) !== 'xml') {
		    Log::error("Invalid file type: $file. Please provide an XML file.");
		    $this->error("Invalid file type: $file. Please provide an XML file.");
		    return 1;
	    }
	    
	    try {
		    $xml = simplexml_load_file($file);
		    
		    foreach ($xml->item as $item) {
			    Coffee::create([
				                 'entity_id' => (int) $item->entity_id,
				                 'category_name' => (string) $item->CategoryName,
				                 'sku' => (string) $item->sku,
				                 'name' => (string) $item->name,
				                 'description' => (string) $item->description,
				                 'shortdesc' => (string) $item->shortdesc,
				                 'price' => (float) $item->price,
				                 'link' => (string) $item->link,
				                 'image' => (string) $item->image,
				                 'brand' => (string) $item->Brand,
				                 'rating' => (int) $item->Rating,
				                 'caffeine_type' => (string) $item->CaffeineType,
				                 'count' => (int) $item->Count,
				                 'flavored' => (string) $item->Flavored === 'Yes',
				                 'seasonal' => (string) $item->Seasonal === 'Yes',
				                 'instock' => (string) $item->Instock === 'Yes',
				                 'facebook' => (string) $item->Facebook === '1',
				                 'is_kcup' => (string) $item->IsKCup === '1',
			                 ]);
		    }
	    } catch (\Exception $e) {
		    Log::error("Error processing file: " . $e->getMessage());
		    $this->error("Error processing file: " . $e->getMessage());
		    return 1;
	    }
	    
	    $this->info('XML file processed successfully.');
	    return 0;
    }
}
