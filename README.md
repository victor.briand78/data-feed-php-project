# Data Feed Processor

## Overview

This project is a command-line tool designed to process a local XML file (`feed.xml`) and push the data to a database. It is built using PHP and the Laravel framework, ensuring easy extendability and configurability.

## Features

- Parses XML files and extracts data.
- Pushes extracted data to a configurable database (default is MySQL).
- Handles errors and logs them to a logfile.
- Easily extendable for different data storage mechanisms.
- Tested with PHPUnit to ensure reliability.

## Requirements

- PHP 7.4 or higher
- Composer
- Laravel 8 or higher
- MySQL

## Setup

### 1. Clone the Repository

```sh
git clone https://gitlab.com/victor.briand78/data-feed-php-project.git
cd data-feed-php-project
```

### 2. Install Dependencies

```sh
composer install
```

### 3. Configure Environment Variables

```sh
cp .env.example .env
```

Set up your database configuration in the .env file

### 4. Run Migrations

```sh
php artisan migrate
```

## Usage

To process the XML file and push data to the database, use the following Artisan command:

```sh
php artisan xml:process /path/to/your-feed-file.xml
```

If the file is not found or is not a valid XML file, an error will be logged, and a message will be displayed.

## Logging

Errors encountered during the execution of the command are logged to storage/logs/laravel.log. The logs configuration can also be changed and specified in `config/logging.php`

## Testing

### 1. Configure Test Environment

```sh
cp .env.testing.example .env.testing
```

Set up your database configuration in the .env.testing file

### 2. Run Tests

To run the tests, use the following command:
```sh
php artisan test
```

## Extending the Application

This application is designed to be easily extendable. To change the data storage mechanism, update the database configuration in the .env file. Laravel supports various database systems such as MySQL, PostgreSQL, SQL Server, and SQLite.